# Python Lecture Series

This repository is meant to accompany a Python lecture series and
provide simple but runnable examples for each topic covered.

### Setup 
You just need to be able to run python 3.6
* run `python -m venv env`
* run `pip install -r requirements.txt`
* run `source env/bin/activate`

### Sample Runs

* run `python lessons/generators_coroutines/coroutines.py`

### Contact

* marcelino.franco@swapoolabs.com
