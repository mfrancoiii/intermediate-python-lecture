"""
    Demonstrates why mutable parameters are bad
"""


def func(container=[]):
    container.append(1)
    print(f"Container contents {container}")


if __name__ == "__main__":
    func()
    func()
    func(container=[5, 4, 3, 2])
    func()
