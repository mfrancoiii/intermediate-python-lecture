"""
    Module to be used to demonstrate import time executions
"""
print("This will always run whether file was imported or executed")


class ImportSample1:
    print("Importing Sample1")

    def print(self):
        print("Printing ImportSample1.print")


class ImportSample2:
    print("Importing Sample2")

    def print(self):
        print("Printing ImportSample2.print")


class ImportSample(ImportSample2, ImportSample1):
    print("Importing ImportSample")


if __name__ == "__main__":
    print("This will only print if this file is executed NOT imported")
