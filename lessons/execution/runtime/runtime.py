"""
    Demonstrates import time vs runtime executions
    Shows how method resolution order works
"""
from mod_runtime import ImportSample


if __name__ == "__main__":
    sample = ImportSample()
    print("Calling print")
    sample.print()
    print(f"{type(sample).__mro__}")
