"""
    Demonstrates coroutines
"""


def printer():
    print("Priming printer")
    while True:
        s = yield
        print(f"Printing value {s}")


if __name__ == "__main__":
    coro = printer()
    next(coro)
    coro.send("msg 1")
    coro.send("msg 2")
    coro.close()
