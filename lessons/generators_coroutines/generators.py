"""
    Demonstrates generators
"""


def func():
    for i in range(5):
        return i


def effective_func():
    return [i for i in range(5)]


def generator():
    for i in range(5):
        yield i


def counter():
    i = 0
    while True:
        yield i
        i += 1


if __name__ == "__main__":
    print(f"func returned: {func()}")
    print(f"effective_func returned: {effective_func()}")
    print(f"generator returned: {generator()}")
    g = generator()
    print(f"next(g): {next(g)}")
    print(f"next(g): {next(g)}")
    print(f"next(g): {next(g)}")
    print(f"next(g): {next(g)}")
    print(f"next(g): {next(g)}")
    try:
        print(f"next(g): {next(g)}")
    except StopIteration as e:
        print(f"{type(e).__name__}: generator() was exhausted")
    g = generator()
    for i in g:
        print(f"looping through generator(): {i}")
    g = counter()
    # counter will run forever so we need to limit the manual drives
    for i in range(10):
        print(f"drive counter {next(g)}")
    # note counter resumes from where it stopped
    print(f"manually drive counter {next(g)}")
