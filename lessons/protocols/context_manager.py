"""
    Demonstrates context managers always executing their exit methods
"""


class Cman:
    def __enter__(self):
        print("Entering Context")

    def __exit__(self, exc_type, exc_val, exc_tb):
        print("Leaving Context")


def func():
    with Cman():
        return 0


if __name__ == "__main__":
    try:
        with Cman():
            raise ValueError
    except ValueError:
        print("ValueError was raised")

    print(f"func returned: {func()}")
