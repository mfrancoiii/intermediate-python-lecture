"""
    Demonstrates magic methods and the descriptor protocol
"""


class ListAttribute:
    def __get__(self, instance, owner):
        return instance.__dict__[self.name]

    def __set__(self, instance, value):
        if not isinstance(value, list):
            raise TypeError(
                f"{type(instance).__name__}.{self.name} only supports 'list' "
                f"types not '{type(value).__name__}'"
            )
        instance.__dict__[self.name] = value

    def __set_name__(self, owner, name):
        self.name = name


class Vector:
    values = ListAttribute()

    def __init__(self, values=None):
        self.values = values or []

    def __getitem__(self, index):
        return self.values[index]

    def __add__(self, other):
        if not isinstance(other, type(self)):
            raise TypeError(
                f"unsupported operand type(s) for +: '{type(self).__name__}' "
                f"and '{type(other).__name__}'"
            )
        return Vector(values=[x + y for x, y in zip(self, other)])

    def __repr__(self):
        return f"{type(self).__name__}(values={self.values})"


if __name__ == "__main__":
    try:
        Vector(values=1)
    except TypeError as e:
        print(f"{type(e).__name__}: {e}")
    try:
        Vector(values="yolo")
    except TypeError as e:
        print(f"{type(e).__name__}: {e}")

    a = Vector(values=[1, 2, 3])
    b = Vector(values=[3, 2, 1])
    c = a + b
    print(f"a: {a}\nb: {b}\nc: {c}")
    for i in c:
        print(i)

    print(f"a first element: {a[0]}")
    print(f"a last element: {a[-1]}")
    print(f"a first 2 elements: {a[:2]}")
    print(f"a reverse: {a[::-1]}")

    try:
        b + 1
    except TypeError as e:
        print(f"{type(e).__name__}: {e}")
